package com.webservice.resource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.webservice.bean.Employee;
import com.webservice.connection.JDBC;

@Path("/employee")
public class Resource {

    @GET
    @Path("/getValues")
    @Produces(MediaType.APPLICATION_XML)
    public List<Employee> getData() {
        List<Employee> employeeList = new ArrayList<Employee>();
        Connection conn = JDBC.getConnection();
        if(conn != null) {
            String selectQuery = "SELECT * FROM employee";
            PreparedStatement preparedStatement;
            try {
                preparedStatement = (PreparedStatement) conn.prepareStatement(selectQuery);
                ResultSet result = preparedStatement.executeQuery();
                
                while(result.next()) {
                    Employee employee = new Employee();
                    int age = result.getInt("age");
                    employee.setAge(age);
                    String name = result.getString("name");
                    employee.setName(name);
                    int empId = result.getInt("empId");
                    employee.setEmpId(empId);
                    String designation = result.getString("designation");
                    employee.setDesignation(designation);
                    employeeList.add(employee);
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("Database Error");
        }
        return employeeList;
    }
    
    @GET
    @Path("/setValues/{age}/{name}/{empId}/{designation}")
    public String setData(@PathParam("age") int age, @PathParam("name")String name,@PathParam("empId") int empId, @PathParam("designation") String designation) {
        String msg = "";
        Connection conn = JDBC.getConnection();
        if(conn != null) {
            String insertQuery = "INSERT INTO employee(age, name, empId, designation) VALUES (?,?,?,?)";
            PreparedStatement preparedStatement;
            try {
                preparedStatement = (PreparedStatement) conn.prepareStatement(insertQuery);
                preparedStatement.setInt(1, age);
                preparedStatement.setString(2, name);
                preparedStatement.setInt(3, empId);
                preparedStatement.setString(4, designation);
                
                int rowUpdated = preparedStatement.executeUpdate();
                if(rowUpdated>0) {
                    msg = "A new user was inserted successfully!";
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            msg = "Database Error";
        }
        return msg;
    }
    
    @GET
    @Path("/deleteValue/{empId}")
    public String deleteData(@PathParam("empId") int empId) {
        String msg = "";
        Connection conn = JDBC.getConnection();
        if(conn != null) {
            String deleteQuery = "DELETE FROM employee WHERE empId = ?";
            PreparedStatement preparedStatement;
            try {
                preparedStatement = (PreparedStatement) conn.prepareStatement(deleteQuery);
                preparedStatement.setInt(1, empId);
                int rowUpdated = preparedStatement.executeUpdate();
                if(rowUpdated>0) {
                    msg = "A user was deleted successfully!";
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            msg = "Database Error";
        }
        return msg;
    }
    
    @GET
    @Path("/updateValue/{empId}/{designation}")
    public String updateData(@PathParam("empId") int empId, @PathParam("name") String designation) {
        String msg = "";
        Connection conn = JDBC.getConnection();
        if(conn != null) {
            String deleteQuery = "UPDATE employee SET designation = ?  WHERE empId = ?";
            PreparedStatement preparedStatement;
            try {
                preparedStatement = (PreparedStatement) conn.prepareStatement(deleteQuery);
                preparedStatement.setString(1, designation);
                preparedStatement.setInt(2, empId);
                int rowUpdated = preparedStatement.executeUpdate();
                if(rowUpdated>0) {
                    msg = "An existing user was updated successfully!";
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else {
            msg = "Database Error";
        }
        return msg;
    }
    
}
