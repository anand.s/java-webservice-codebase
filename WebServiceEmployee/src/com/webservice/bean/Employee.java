package com.webservice.bean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Employee {
private int age;
private int empId;
private String name;
private String designation;

public int getAge() {
    return age;
}

public void setAge(int age) {
    this.age = age;
}

public String getName() {
    return name;
}

public int getEmpId() {
    return empId;
}

public void setEmpId(int empId) {
    this.empId = empId;
}

public String getDesignation() {
    return designation;
}

public void setDesignation(String designation) {
    this.designation = designation;
}

public void setName(String name) {
    this.name = name;
}
}
