package com.webservice.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBC {

    private JDBC() {

    }

    public static Connection getConnection() {
        Connection connection = null;
        if (connection == null) {
            System.out.println("-------- SqLite JDBC Connection Testing ------------");

            try {
                Class.forName("org.sqlite.JDBC");
                System.out.println("SqLite JDBC Driver Registered!");
                
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            try { 
                connection = (Connection) DriverManager.getConnection("jdbc:sqlite:/tmp/employeeschema.db");
                System.out.println("SqLite Connection Established!");
            } catch (Exception e) {
                System.out.println("Connection Failed!");
                e.printStackTrace();
            }

        }
        return connection;
    }

}
